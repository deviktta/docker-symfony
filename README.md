# Docker for Symfony

This Docker template is meant to run Symfony applications, providing up-to-date software and tools to manage it.

**Docker container set-up for Symfony 5.3:**
* Debian 11 "Bullseye" with Apache 2
* PHP 8.0
* MariaDB 10.6
* Phpmyadmin 5.1

## Major releases
- [v5.3.4](https://gitlab.com/deviktta/docker-for-symfony/-/releases/5.3.4): latest release for Symfony 5.3 (Debian 11, PHP 8.0, MariaDB 10.6, phhpMyAdmin 5.1).

## How to use this Docker template

### Requirements:
The only requirement is to have installed the last version of both [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

### Installation:
1. Clone this project from Gitlab (`git clone https://gitlab.com/deviktta/docker-for-symfony.git`) or [download a release](https://gitlab.com/deviktta/docker-for-symfony/-/releases).

2. Bring up the containers: `docker-compose up -d --build`

3. Install your Symfony app:
- By cloning your Symfony project into /symfony-root folder, making sure your composer.json is located in the root level.
- And then, installing Symfony (read carefully your project installation instructions), usually by:
  - Downloading Composer dependencies `./docker/scripts/composer install`
  - Updating the database: `./docker/scripts/console doctrine:migrations:migrate`
  - Setting up .env:`nano ./symfony-root/.env`

Or, if you wish, you can create a new Symfony app through Composer by running `./docker/scripts/symfony new .`

4. Visit your site by navigating to http://localhost.

### Managing your Symfony application:
**Available tools:**
* Composer: `./docker/scripts/composer`
* Symfony Console: `./docker/scripts/console`
* Symfony cli: `./docker/scripts/symfony`
* PHP Code Sniffer: `./docker/scripts/phpcs`
* PhpStan: `./docker/scripts/phpstan`

**PHP config:**
* php.ini: `/docker/phpserver/php.ini`.
* opcache: `/docker/phpserver/opcache.ini`.
* xdebug: `/docker/phpserver/xdebug.ini`; default settings: idekey=xdebug / port=9003.

**Apache config:**
* Virtual hosts: `/docker/phpserver/sites-enabled/000-default.conf`

Application logs can be found in `/logs` directory.

Overriding your Docker environment file (copy `/.env` file to `/.env.local`) and customizing the values is advised.

## License
This project is under [MIT License](https://gitlab.com/deviktta/docker-for-symfony/-/blob/master/LICENSE.txt).

## About
Contact e-mail: [deviktta@protonmail.com](mailto:deviktta@protonmail.com); made with love from Barcelona, Catalunya.
